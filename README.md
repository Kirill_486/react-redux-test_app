#  React Redux test APP

## API

### List persons
```
listPersonModels()
personModelsHelper.ts:13 {
  "id": 0,
  "firstName": "Vasia",
  "lastName": "Petyrov",
  "comment": "Ptrov Vasya",
  "fullName": "Vasia Petyrov"
}
personModelsHelper.ts:13 {
  "id": 1,
  "firstName": "Vasia2",
  "lastName": "Ivanov",
  "comment": "Uasiliy Ivanov",
  "fullName": "Vasia2 Ivanov"
}
```

### AddPersonModel 

```
addPersonModel(person)

listPersonModels()
personModelsHelper.ts:13 {
  "id": 0,
  "firstName": "Vasia",
  "lastName": "Petyrov",
  "comment": "Ptrov Vasya",
  "fullName": "Vasia Petyrov"
}
personModelsHelper.ts:13 {
  "id": 1,
  "firstName": "Vasia2",
  "lastName": "Ivanov",
  "comment": "Uasiliy Ivanov",
  "fullName": "Vasia2 Ivanov"
}
personModelsHelper.ts:13 {
  "id": 2,
  "firstName": "Somename",
  "lastName": "Surname",
  "comment": "thirdPerson",
  "fullName": "Somename Surname"
}
```

### Add person JSON

```
let p2 = person
p2.id = 3
addPersonFromJson(JSON.stringify(p2))


{
  "id": 0,
  "firstName": "Vasia",
  "lastName": "Petyrov",
  "comment": "Ptrov Vasya",
  "fullName": "Vasia Petyrov"
}
personModelsHelper.ts:13 {
  "id": 1,
  "firstName": "Vasia2",
  "lastName": "Ivanov",
  "comment": "Uasiliy Ivanov",
  "fullName": "Vasia2 Ivanov"
}
personModelsHelper.ts:13 {
  "id": 2,
  "firstName": "Somename",
  "lastName": "Surname",
  "comment": "thirdPerson",
  "fullName": "Somename Surname"
}
personModelsHelper.ts:13 {
  "id": 3,
  "firstName": "Somename",
  "lastName": "Surname",
  "comment": "thirdPerson",
  "fullName": "Somename Surname"
}

```