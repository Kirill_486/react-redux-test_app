import * as React from 'react';
import {connect} from 'react-redux';
import store from './store/configureStore';
import FullNameForm, {} from './hoc/FullNameForm';
import PartsNameForm from './hoc/PrtsNameForm';
import './App.css';

import logo from './logo.svg';
import { mergeFullName } from './store/actions/personFormActions';
import { IPerson, IApplicationState } from './typings/store';
import { thunkRestoreModel } from './store/actions/personFormThunkActions';
import { addPersonFromJson, addPersonModel, logPersonModel, listPersonModels } from './store/helpers/personModelsHelper';

// tslint:disable-next-line:no-var-requires
const initialPersons: IPerson[] = require('json-loader!./raw/data.json');
for (const person of initialPersons) {
  store.dispatch(thunkRestoreModel(person));
}

const root = window as any;

root.addPersonFromJson = addPersonFromJson;
root.addPersonModel = addPersonModel;
root.logPersonModel = logPersonModel;
root.listPersonModels = listPersonModels;

const thirdPerson: IPerson = {
  id: 2,
  firstName: 'Somename',
  lastName: 'Surname',
  comment: 'thirdPerson'
};
root.person = thirdPerson;

let isInitial = true;

export const app = () => {
  
  React.useEffect(()=> {
    if (isInitial) {
      store.dispatch(mergeFullName());
      isInitial = false;
    }
  });
  
  return (
    <div className="App">
      <header className="App__header">
        <img src={logo} className="App-logo" alt="logo" />
        <h1 className="App-title">Welcome to React</h1>
      </header>
      <div className="App__body">
        {
          store.getState().personPool.map((person) => {
            return (
              <div 
                className={`person-forms_${person.id}`}
                key={person.id}
              >
                <FullNameForm id={person.id} />
                <PartsNameForm id={person.id} />
              </div>
            );
            
          })
        }
        
      </div>        
    </div>
  )
}

const mapStateToProps =
(
  state: IApplicationState
) => {
  return {
    persons: state.personPool
  }
}

const connectedApp = connect(mapStateToProps)(app);

export default connectedApp;
