import * as React from 'react';
import { IFullNamePersonFormProps } from 'src/typings/components';

export const FullNamePersonForm: React.SFC<IFullNamePersonFormProps> = (props) => {
    return (
        <div className="full-name__container">
            <h3>FullNamePersonForm</h3>
            <label htmlFor="full-name__input">Full Name</label>
            <input
                id="full-name__input"
                type="text" 
                className="full-name__input"
                onChange={props.onFullNameChange}
                value={props.fullName || ''}
                />
            <label htmlFor="comment__textarea">Comment</label>
            <textarea 
                name="comment" 
                id="comment__textarea" 
                cols={5} 
                rows={10}
                value={props.comment || ''}
                onChange={props.onCommentChange}
            />            
        </div>
    )
}