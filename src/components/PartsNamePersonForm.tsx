import * as React from 'react';
import {IPartsNamePersonFormProps} from '../typings/components'

export const PartsNamePersonForm: React.SFC<IPartsNamePersonFormProps> = (props) => {
    return (
        <div className="parts-name__container">
            <h3>PartsNamePersonForm</h3>
            <label htmlFor="first-name__input">First Name</label>
            <input 
                type="text" 
                id="first-name__input" 
                className="first-name__input"
                onChange={props.onFirstNameChange}
                value={props.firstName}
            />

            <label htmlFor="last-name__input">Last Name</label>
            <input 
                type="text" 
                id="last-name__input" 
                className="last-name__input"
                onChange={props.onLastNameChange}
                value={props.lastName}
            />

            <label htmlFor="comment__textarea">Comment</label>
            <textarea 
                name="comment" 
                id="comment__textarea" 
                cols={5} 
                rows={10}
                value={props.comment}
                onChange={props.onCommentChange}
            />            
        </div>
    )
}