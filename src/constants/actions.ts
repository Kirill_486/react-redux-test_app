export enum personFormActionTypes {
    createNewPerson = 'createNewPerson',
    firstNameChange = 'firstNameChange',
    lastNameChange = 'lastNameChange',
    fullNameChange = 'fullNameChange',
    commentChange = 'commentChange',
    mergeFullName = 'mergeFullName',
    splitFullName = 'splitFullName'
}
