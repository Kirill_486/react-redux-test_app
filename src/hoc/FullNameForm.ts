import { IFullNamePersonFormStateProps, IFullNamePersonFormDispatchProps, IPersonFormOwnProps } from './../typings/components';
import {FullNamePersonForm} from '../components/FullNamePersonForm';
import { IApplicationState } from 'src/typings/store';
import { thunkFullNameChanged, thunkCommentChanged, compoundAction } from 'src/store/actions/personFormThunkActions';
import { connect } from 'react-redux';
import { IEventWithTarget } from 'src/typings/misc';
import { selectModel } from 'src/store/selectors/personFormSelector';

const mapStateToProps = 
(
    state: IApplicationState,
    props: IPersonFormOwnProps
): IFullNamePersonFormStateProps => {
    const {fullName, comment} = selectModel(state, props.id);
    return {
        fullName,
        comment
    }
}

const mapDispatchToProps = 
(
    dispatch: any,
    props: IPersonFormOwnProps
): IFullNamePersonFormDispatchProps => {
    return {
        onFullNameChange: (e: IEventWithTarget) => dispatch(compoundAction(props.id, thunkFullNameChanged, e.target.value)),
        onCommentChange: (e: IEventWithTarget) => dispatch(compoundAction(props.id, thunkCommentChanged, e.target.value))         
    }
}

const FullNameForm = connect(mapStateToProps, mapDispatchToProps)(FullNamePersonForm);
FullNameForm.displayName = 'FullNameForm';

export default FullNameForm;