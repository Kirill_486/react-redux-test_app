import { selectModel } from 'src/store/selectors/personFormSelector';
import { IApplicationState } from 'src/typings/store';
import { connect } from 'react-redux';
import {PartsNamePersonForm} from '../components/PartsNamePersonForm';
import { IPartsNamePersonFormStateProps, IPartsNamePersonFormDispatchProps, IPersonFormOwnProps } from 'src/typings/components';
import { thunkFirstNameChanged, thunkLastNameChanged, thunkCommentChanged, compoundAction } from 'src/store/actions/personFormThunkActions';
import { IEventWithTarget } from 'src/typings/misc';

const mapStateToProps = 
(
    state: IApplicationState,
    props: IPersonFormOwnProps 
): IPartsNamePersonFormStateProps => {
    const {firstName, lastName, comment} = selectModel(state, props.id);
    return {
        firstName,
        lastName,
        comment
    }
};

const mapDispatchToProps = 
(
    dispatch: any,
    props: IPersonFormOwnProps
): IPartsNamePersonFormDispatchProps => {
    return {
        onFirstNameChange: (e: IEventWithTarget) => dispatch(compoundAction(props.id, thunkFirstNameChanged, e.target.value)),
        onLastNameChange: (e: IEventWithTarget) => dispatch(compoundAction(props.id, thunkLastNameChanged, e.target.value)),
        onCommentChange: (e: IEventWithTarget) => dispatch(compoundAction(props.id, thunkCommentChanged, e.target.value))
    }
}

const PartsNameForm = connect(mapStateToProps, mapDispatchToProps)(PartsNamePersonForm);
PartsNameForm.displayName = 'PartsNameForm';

export default PartsNameForm;