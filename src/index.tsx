import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './App';
import {Provider} from 'react-redux';
import store from './store/configureStore';
import './index.css';
import registerServiceWorker from './registerServiceWorker';

const jsx = (
  <Provider store={store}>
    <App />
  </Provider>
);

ReactDOM.render(
  jsx,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
