import { IPersonModelAction } from './../../typings/actions';
import {ActionCreator} from 'redux'
import { personFormActionTypes } from 'src/constants/actions';
import { IPersonStringAction, IPersonAction } from 'src/typings/actions';
import { IPerson } from 'src/typings/store';

export const firstNameChange: ActionCreator<IPersonStringAction> = 
(
    id: number,
    newValue: string
) => {
    return {
        type: personFormActionTypes.firstNameChange,
        id,
        payload: newValue
    }
};

export const lastNameChanged: ActionCreator<IPersonStringAction> =
(
    id: number,
    newValue: string
) => {
    return {
        type: personFormActionTypes.lastNameChange,
        id,
        payload: newValue
    }
}

export const fullNameChanged: ActionCreator<IPersonStringAction> =
(
    id: number,
    newValue: string
) => {
    return {
        type: personFormActionTypes.fullNameChange,
        id,
        payload: newValue
    }
}

export const commentChanged: ActionCreator<IPersonStringAction> =
(
    id: number,
    newValue: string
) => {
    return {
        type: personFormActionTypes.commentChange,
        id,
        payload: newValue
    }
}

export const mergeFullName: ActionCreator<IPersonAction> =
(
    id: number
) => {
    return {
        type: personFormActionTypes.mergeFullName,
        id
    }
}

export const splitFullName: ActionCreator<IPersonAction> =
(
    id: number
) => {
    return {
        type: personFormActionTypes.splitFullName,
        id
    }
}

export const createNewPerson: ActionCreator<IPersonModelAction> =
(
    model: IPerson
) => {
    return {
        type: personFormActionTypes.createNewPerson,
        payload: model
    }
}
