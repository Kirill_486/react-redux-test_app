import { calcPersonDiff } from './../selectors/personReducerSelector';
import {ActionCreator} from 'redux'
import { fullNameChanged, lastNameChanged, mergeFullName, splitFullName, commentChanged, firstNameChange, createNewPerson } from './personFormActions';
import { IPerson } from 'src/typings/store';
import store, { getAppState } from '../configureStore';
import { logPerson } from '../helpers/personModelsHelper';

export const compoundAction: ActionCreator<any> =
(
    id: number,
    actionCreator: ActionCreator<any>,
    newValue: string
) => {

    const action = (dispatch: any) => {
        const innerCompoundAction = userChangedPerson(id, actionCreator(id, newValue));
        dispatch(innerCompoundAction);
    }

    return action;
}

export const userChangedPerson: ActionCreator<any> =
(
    id: number,
    innerAction: any
) => {
    const action = (dispatch: any) => {
        // it exist becouse it got  changed
        const itemBeforeActions = getAppState().personPool.find((item) => item.id === id) as IPerson;

        dispatch(innerAction);

        // it exist becouse it got  changed
        const itemAfterActions = getAppState().personPool.find((item) => item.id === id) as IPerson;
        const diff = calcPersonDiff(itemBeforeActions, itemAfterActions) as IPerson;
        logPerson(diff);
    }

    return action;
}

export const thunkFirstNameChanged: ActionCreator<any> =
(
    id: number,
    newValue: string
) => {
    const action = (dispatch: any) => {
        // it exist becouse we change its name
        dispatch(firstNameChange(id, newValue));
        dispatch(mergeFullName(id));
    }
    return action;
}

export const thunkLastNameChanged: ActionCreator<any> =
(
    id: number,
    newValue: string
) => {
    const action = (dispatch: any) => {
        dispatch(lastNameChanged(id, newValue));
        dispatch(mergeFullName(id));        
    }

    return action;
};

export const thunkFullNameChanged: ActionCreator<any> =
(
    id: number,
    newValue: string
) => {
    const action = (dispatch: any) => {
        dispatch(fullNameChanged(id, newValue));
        dispatch(splitFullName(id));        
    }
    return action;
}

export const thunkCommentChanged: ActionCreator<any> =
(
    id: number,
    newValue: string
) => {
    const action = (dispatch: any) => {
        dispatch(commentChanged(id, newValue));        
    }

    return action;
}

export const thunkRestoreModel: ActionCreator<any> =
(
    model: IPerson
) => {
    const action = (dispatch: any) => {
        const appState = store.getState();

        const isNewModel = !appState.personPool.find((item) => item.id === model.id);

        if (isNewModel) {
            dispatch(createNewPerson(model));
        } else {
            const {id, firstName, lastName, comment} = model;
            dispatch(firstNameChange(id, firstName));
            dispatch(lastNameChanged(id, lastName));
            dispatch(commentChanged(id, comment));
        }       

        dispatch(mergeFullName(model.id));
    }

    return action;
}