import { createStore, combineReducers } from 'redux';
import { applyMiddleware } from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import {personFormReducer} from './reducers/personFormReducer';

// const store = createStore(personFormReducer, composeWithDevTools(applyMiddleware(thunk)));
const store = createStore(combineReducers({
    personPool: personFormReducer    
}), composeWithDevTools(applyMiddleware(thunk)));
export default store;

export const getAppState = () => store.getState();