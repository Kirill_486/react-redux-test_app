import { IPerson } from "src/typings/store";

export const getDefaultPerson = (): IPerson => {
    return {
        id: 0,
        firstName: 'John',
        lastName: 'Doe',
        comment: 'Apparently homeless'
    }
};

export const personKeys = Object.keys(getDefaultPerson());
