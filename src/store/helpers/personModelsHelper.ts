import store, { getAppState } from '../configureStore';
import { thunkRestoreModel } from '../actions/personFormThunkActions';
import { IPerson } from 'src/typings/store';
import { personKeys } from '../defaults';

export const isPerson = (model: any): boolean => {
    const {id, firstName, lastName} = model;
    
    return (typeof id === 'number') && firstName && lastName;
}

const castToPerson = (model: any): IPerson => {
    const {id, firstName, lastName, comment} = model;
    if (typeof id !== 'number') {
        throw new Error('Person MUST have id');
    }
    return {
        id,
        firstName: firstName || '',
        lastName: lastName || '',
        comment: comment || ''
    }
}

export const logPerson = (model: IPerson) => {
    // tslint:disable-next-line:no-console
    console.log(JSON.stringify(model, undefined, 2));
}

export const addPersonFromJson =
(
    json: string
) => {
    const passedObject = JSON.parse(json);
    addPersonModel(passedObject);    
}

const cutModel = (model: any) => {
    const result = {};
    for (const key of personKeys) {
        // no way we rewrite the id
        if (model[key] && (model[key] !== 'id')) {
            result[key] = model[key]
        }
    }
    return result;
}

export const mergePersonAndModel =
(
    person: IPerson,
    model: any
) => {
    
    return {
        ...person,
        ...cutModel(model)
    };
}

export const addPersonModel =
(
    model: any
): void => {

    try {
        const id = model.id;
        const existingPerson = getAppState().personPool.find((item) => item.id === id );

        if (existingPerson) {
            store.dispatch(thunkRestoreModel(mergePersonAndModel(existingPerson, model)));
        } else {
            const person = castToPerson(model);
            store.dispatch(thunkRestoreModel(person))
        }

        
    } catch (e) {
        // tslint:disable-next-line:no-console
        console.error(e);
    }
}

export const logPersonModel = (
    id: number
): void => {
    const model = store.getState().personPool.find((item) => item.id === id);

    if (model) {
        logPerson(model);
    }    
}

export const listPersonModels = () => {
    const personPool = store.getState().personPool;
    for (const person of personPool) {
        logPerson(person);
    }
}