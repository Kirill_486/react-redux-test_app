import { IPersonModelAction } from './../../typings/actions';
import { personFormActionTypes } from 'src/constants/actions';
import { IPersonAction, IPersonStringAction } from 'src/typings/actions';
import { IPersonPoolState, IPerson } from 'src/typings/store';
import { 
    firstNameChangedSelector, 
    lastNameChangedSelector, 
    fullNameChangedSelector, 
    commentChangedSelector, 
    splitFullNameSelector, 
    mergeFullNameSelector, 
    createPersonSelector } from '../selectors/personReducerSelector';

const initialState: IPersonPoolState = [] as IPerson[];

export const personFormReducer = 
(
    state: IPersonPoolState = initialState,
    action: IPersonModelAction | IPersonAction | IPersonStringAction
) => {
    switch (action.type) {
        case personFormActionTypes.createNewPerson: {
            const modelAction = action as IPersonModelAction;
            return createPersonSelector(state, modelAction);
        }
        case personFormActionTypes.firstNameChange: {
            const stringAction = action as IPersonStringAction;
            return firstNameChangedSelector(state, stringAction);
        }
        case personFormActionTypes.lastNameChange: {
            const stringAction = action as IPersonStringAction;
            return lastNameChangedSelector(state, stringAction);
        }
        case personFormActionTypes.fullNameChange: {
            const stringAction = action as IPersonStringAction;
            return fullNameChangedSelector(state, stringAction);
        }
        case personFormActionTypes.commentChange: {
            const stringAction = action as IPersonStringAction;
            return commentChangedSelector(state, stringAction);
        }
        case personFormActionTypes.mergeFullName: {
            const personAction = action as IPersonAction;
            return mergeFullNameSelector(state, personAction);
        }
        case personFormActionTypes.splitFullName: {
            const personAction = action as IPersonAction;
            return splitFullNameSelector(state, personAction);
        }
        default: return state;

    }
}