import { ISplitFullNameResult } from "src/typings/misc";

export const mergeFullName = 
(
    firstName: string,
    lastName: string
): string => {
    return `${firstName} ${lastName}`;
};

export const splitFullName = 
(
    fullName: string
): ISplitFullNameResult => {
    const nameParts = fullName.split(' ');
    const namePartsLength = nameParts.length;
    let firstName = '';
    let lastName = '';

    if (namePartsLength < 2) {
        firstName = nameParts[0];        
    } else if (namePartsLength === 2) {
        firstName = nameParts[0];
        lastName = nameParts[1];
    } else if (namePartsLength > 2) {
        // nameParts > 2
        firstName = nameParts[0];
        // the line below is becouse I know better that nameParts.length > 1
        // undefined value is not possible
        // but thank you, ts for your concern)))
        // const lastNameParts = (nameParts.shift() as unknown) as string[];
        nameParts.shift();
        lastName = nameParts.join(' ');
    } else {
        throw new Error("Something crazy's happening"); 
    }
    return {
        firstName,
        lastName
    }
}