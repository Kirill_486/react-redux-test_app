import { IApplicationState, IPerson } from "../../typings/store";
import { getDefaultPerson } from '../defaults';

export const selectModel = (
    state: IApplicationState,
    id: number
): IPerson => {
    const model = state.personPool.find((item) => item.id === id);

    return model ? model : getDefaultPerson()
}

export const logModelToJSON = 
(
    state: IApplicationState,
    id: number
): void => {
    const model = selectModel(state, id);
    // tslint:disable-next-line:no-console
    console.log(JSON.stringify(model));
}