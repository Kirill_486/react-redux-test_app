import { IPersonPoolState, IPerson } from './../../typings/store';
import { IPersonStringAction, IPersonAction, IPersonModelAction } from '../../typings/actions';
import { mergeFullName, splitFullName } from './namePartsSelector';

export const createPersonSelector =
(
    state: IPersonPoolState,
    action: IPersonModelAction
) => {
    return [
        ...state,
        action.payload
    ]
}

export const firstNameChangedSelector =
(
    state: IPersonPoolState, 
    action: IPersonStringAction
): IPersonPoolState => {
    
    const id = action.id;
    const firstName = action.payload;

    return state.map((item)=> {
        if (item.id !== id) {
            return item;
        } else {
            const targetItem = item;
            
            return {
                ...targetItem,
                firstName
            }
        }
    })
}

export const lastNameChangedSelector =
(
    state: IPersonPoolState, 
    action: IPersonStringAction
): IPersonPoolState => {

    const id = action.id;
    const lastName = action.payload;

    return state.map((item)=> {
        if (item.id !== id) {
            return item;
        } else {

            const targetItem = item;
            return {
                ...targetItem,
                lastName
            }
        }
    })
}

export const fullNameChangedSelector =
(
    state: IPersonPoolState, 
    action: IPersonStringAction
): IPersonPoolState => {

    const id = action.id;
    const fullName = action.payload;

    return state.map((item)=> {
        if (item.id !== id) {
            return item;
        } else {
            const targetItem = item;
            
            return {
                ...targetItem,
                fullName
            }
        }
    })
}

export const commentChangedSelector =
(
    state: IPersonPoolState, 
    action: IPersonStringAction
): IPersonPoolState => {

    const id = action.id;
    const comment = action.payload;

    return state.map((item)=> {
        if (item.id !== id) {
            return item;
        } else {
            const targetItem = item;
            
            return {
                ...targetItem,
                comment
            }
        }
    })
}

export const mergeFullNameSelector =
(
    state: IPersonPoolState, 
    action: IPersonAction
): IPersonPoolState => {
    const id = action.id;
    return state.map((item)=> {
        
        if (item.id !== id) {
            return item;
        } else {
            const targetItem = item;
            const fullName = mergeFullName(targetItem.firstName, targetItem.lastName);
            return {
                ...targetItem,
                fullName
            }
        }
    });
}

export const splitFullNameSelector =
(
    state: IPersonPoolState,
    action: IPersonAction
): IPersonPoolState => {
    const id = action.id;
    return state.map((item) => {
        if (item.id !== id) {
            return item;
        } else {
            const targetItem = item;
            const {firstName, lastName} = splitFullName(targetItem.fullName || '');
            return {
                ...targetItem,
                firstName,
                lastName
            }
        }
    });
}

export const calcPersonDiff = 
(
    personBefore: IPerson,
    personAfter: IPerson
) => {
    const diff = {};
    for (const key in personBefore) {
        if (personBefore[key] !== personAfter[key]) {
            diff[key] = personAfter[key]
        }
    }
    return diff;
}
