import { Action } from 'redux';
import { IPerson } from './store';

export interface IPersonAction extends Action {
    id: number;
}

export interface IPersonStringAction extends IPersonAction {
    payload: string;
}

export interface IPersonModelAction extends Action {
    payload: IPerson;
}