import { IEventWithTarget } from './misc';

export interface IPersonFormOwnProps {
    id: number;
}

export interface IFullNamePersonFormStateProps {
    fullName?: string;
    comment?: string;
}

export interface IFullNamePersonFormDispatchProps {
    onFullNameChange?: (e: IEventWithTarget) => void;
    onCommentChange?: (e: IEventWithTarget) => void;
}

export interface IFullNamePersonFormProps 
extends IPersonFormOwnProps,
IFullNamePersonFormStateProps, 
IFullNamePersonFormDispatchProps {}

export interface IPartsNamePersonFormStateProps {
    firstName?: string;
    lastName?: string;
    comment?: string;
}

export interface IPartsNamePersonFormDispatchProps {
    onFirstNameChange?: (e: IEventWithTarget) => void;
    onLastNameChange?: (e: IEventWithTarget) => void;
    onCommentChange?: (e: IEventWithTarget) => void;
}

export interface IPartsNamePersonFormProps 
extends IPartsNamePersonFormStateProps, 
IPartsNamePersonFormDispatchProps {}
