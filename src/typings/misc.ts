export interface ISplitFullNameResult {
    firstName: string;
    lastName: string;
}

export interface IEventWithTarget {
    target: {
        value: string
    }
}
