export interface IEntity {
    id: number;
}

export interface IPerson extends IEntity {
    fullName?: string;
    firstName: string;
    lastName: string;
    comment: string;
}

// when state gets more complex remove the line below
// tslint:disable-next-line:no-empty-interface
export type IPersonPoolState = IPerson[];

// Delete the line below after state got more complex
// tslint:disable-next-line:no-empty-interface
export interface IApplicationState extends IPersonPoolState {
    personPool: IPersonPoolState
}